import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxPopper } from 'angular-popper';
import { RangeSliderModule } from 'ngx-range-slider';

import { AppComponent } from './app.component';
import { UiModule } from './ui/ui.module';
import { SliderComponent } from './content/slider/slider.component';
import { ProductsComponent } from './content/products/products.component';
import { ListComponent } from './content/products/list/list.component';

@NgModule({
  declarations: [
    AppComponent,
    SliderComponent,
    ProductsComponent,
    ListComponent
  ],
  imports: [
    BrowserModule,
    UiModule,
    NgxPopper,
    RangeSliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
