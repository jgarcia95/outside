import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

interface Producto {
  name: string;
  price: number;
  points: any;
  image: string;
}

export const DUMMY_DATA = [
  {
    name: 'Product 1',
    price: 1000,
    points: [1,2,3,4],
    image: 'http://placekitten.com/g/200/300'
  },
  {
    name: 'Product 2',
    price: 60,
    points: [1,2,3,4,5],
    image: 'http://placekitten.com/200/300'
  },
  {
    name: 'Product 3',
    price: 30,
    points: [1,2,3],
    image: 'http://placekitten.com/200/300'
  },
  {
    name: 'Product 4',
    price: 200,
    points: [1,2,3,4,5],
    image: 'http://placekitten.com/200/300'
  },
  {
    name: 'Product 5',
    price: 100,
    points: [1,2,3,4],
    image: 'http://placekitten.com/200/300'
  },
  {
    name: 'Product 6',
    price: 1000,
    points: [1,2,3,4,5],
    image: 'http://placekitten.com/200/300'
  },
];

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  private productoSubject = new BehaviorSubject([]);
  private producto: Producto[];

  constructor() { 
    this.producto = DUMMY_DATA;
    this.productoSubject.next(this.producto);
  }

  getProductos(): Observable<Producto[]> {
    return this.productoSubject.asObservable();
  }
}
