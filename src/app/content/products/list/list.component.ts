import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProductosService } from '../../../services/productos.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {

  public productos;
  constructor( private productoService : ProductosService ) { }

  ngOnInit() {
    this.productoService.getProductos().subscribe( data =>{
      this.productos = data;
    });

    console.log( this.productos );
  }

}
