import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass']
})
export class ProductsComponent implements OnInit {

  public show_filter_form : boolean = false;
  selected = 0;
  hovered = 0;
  readonly = false;

  min1=-10;
  max1=50;
  step=5;
  stepRange=[0,40];


  constructor() { }

  ngOnInit() {
  }

  showFilterForm(){
    
    this.show_filter_form = ( this.show_filter_form == false ) ? true : false;
  }

}
